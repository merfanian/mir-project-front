from app.encoding import Encoder, GammaEncoder, convert_bin_str_to_bytes
from abc import ABC, abstractmethod
from base64 import standard_b64encode, standard_b64decode
from typing import Dict, Iterable, List
from math import log
import json
import logging
import numpy as np


logger = logging.getLogger(__name__)


class SearchEngine(ABC):
    def __init__(
        self, docs_path: str, export_path: str, stopwords_path: str, encoder: Encoder
    ):
        self.bigram_db = {}
        self.words_db = set()
        self.encoder = encoder
        self.construct_stopwords_list(stopwords_path)
        self.construct_positional_index(docs_path)
        logger.info(
            f"size of corpus: {len(self.corpus)}, bigram_db: {len(self.bigram_db)}, words_db: {len(self.words_db)}"
        )
        self.save_index(export_path)

    @abstractmethod
    def prepare_text(self, raw_text: str) -> Iterable[str]:
        pass

    @abstractmethod
    def get_corpus(self, docs_path):
        pass

    @abstractmethod
    def correct_query(self, query):
        pass

    @abstractmethod
    def get_page_by_id(self, id):
        pass

    def supports_categorization(self) -> bool:
        return False

    def predict_category(self, doc) -> int:
        pass

    def save_index(self, destination):
        with open(destination, "w") as file:
            if self.encoder:

                def encode_differences(indices: List[int]) -> Iterable[int]:
                    previous = 0
                    for index in indices:
                        yield index - previous
                        previous = index

                def encode_index_list(i_list: List[int]) -> Iterable[int]:
                    if i_list is None:
                        yield 1
                    else:
                        yield len(i_list) + 1

                        si_list = sorted(i_list)
                        for i in range(len(si_list)):
                            si_list[i] += 1

                        for num in encode_differences(si_list):
                            yield num

                def encode_indices(indices: Dict[str, List[int]]) -> Iterable[int]:
                    for section in ["title", "text"]:
                        for num in encode_index_list(indices.get(section)):
                            yield num

                def encode_postings_list(
                    postings_list: Dict[int, Dict[str, List[int]]]
                ) -> Iterable[int]:
                    yield len(postings_list) + 1
                    encoded_docids = encode_differences(
                        sorted(int(id) + 1 for id in postings_list.keys())
                    )
                    for docid in sorted(postings_list.keys()):
                        yield next(encoded_docids)
                        for num in encode_indices(postings_list[docid]):
                            yield num

                def encode_postings_list_base64(
                    postings_list: dict, encoder: Encoder
                ) -> bytes:
                    bin_str = "".join(
                        encoder.encode(num)
                        for num in encode_postings_list(postings_list)
                    )
                    if encoder.has_leading_zeros:
                        bin_str = "1" + bin_str

                    return standard_b64encode(convert_bin_str_to_bytes(bin_str))

                json.dump(
                    {
                        key: encode_postings_list_base64(
                            self.index[key], self.encoder
                        ).decode("utf-8")
                        for key in self.index
                    },
                    file,
                )
            else:
                json.dump(self.index, file)

    def load_index(self, source):
        with open(source) as file:
            index = json.load(file)
            if self.encoder:

                def decode_index_list(encoded) -> List[int]:
                    index_count = next(encoded) - 1
                    if index_count == 0:
                        return []
                    indices = list(next(encoded) for i in range(index_count))
                    indices[0] -= 1
                    cum = 0
                    for i in range(len(indices)):
                        cum += indices[i]
                        indices[i] = cum
                    return indices

                def decode_indices(encoded) -> Dict[str, List[int]]:
                    result = dict()

                    for section in ["title", "text"]:
                        result[section] = decode_index_list(encoded)

                    return result

                def decode_postings_list(encoded) -> Dict[int, Dict[str, List[int]]]:
                    result = dict()

                    doc_count = next(encoded) - 1

                    previous_docid = -1
                    for _ in range(doc_count):
                        docid = next(encoded) + previous_docid
                        previous_docid = docid
                        result[docid] = decode_indices(encoded)

                    return result

                def decode_postings_list_base64(base64_str: str, decoder: Encoder):
                    encoded_bytes = list(standard_b64decode(base64_str))
                    return decode_postings_list(decoder.decode(encoded_bytes))

                index = {
                    key: decode_postings_list_base64(value, self.encoder)
                    for key, value in index.items()
                }

            return index

    def get_words(self):
        words = set()
        for (title, text) in self.corpus.values():
            for word in title:
                words.add(word)
            for word in text:
                words.add(word)
        return words

    def add_to_index(self, doc_id, title, text) -> None:
        if len(title) > 0 and doc_id in self.index[title[0]]:
            logging.error("Oops! Doc Already exists")
            return

        title_idx = 0
        for t in title:
            if doc_id not in self.index[t]:
                self.index[t][doc_id] = {}
            if "title" not in self.index[t][doc_id]:
                self.index[t][doc_id]["title"] = []
            self.index[t][doc_id]["title"].append(title_idx)
            title_idx = title_idx + 1

        text_idx = 0
        for t in text:
            if doc_id not in self.index[t]:
                self.index[t][doc_id] = {}
            if "text" not in self.index[t][doc_id]:
                self.index[t][doc_id]["text"] = []
            self.index[t][doc_id]["text"].append(text_idx)
            text_idx = text_idx + 1

    def construct_stopwords_list(self, path) -> None:
        self.stopwords = []
        with open(path, "r") as f:
            lines = f.readlines()

        self.stopwords = [line.strip() for line in lines]
        logger.debug(f"stopwords are: {str(self.stopwords)}")

    def construct_positional_index(self, docs_path) -> None:
        self.corpus = self.get_corpus(docs_path)
        logging.info("Corpus imported! Constructing words set...")

        words = self.get_words()
        logging.info(f"#Words: {len(words)} Constructing the index ...")

        self.index = {}
        for word in words:
            self.index[word] = {}

        for i in self.corpus:
            (title, text) = self.corpus[i]
            self.add_to_index(i, title, text)

    def get_posting_list(self, word):
        posting_list = self.index[word]
        return posting_list

    def add_word_to_bigram(self, word):
        bigram_db = self.bigram_db
        words_db = self.words_db

        if word in words_db:
            return
        words_db.add(word)
        for i in range(len(word) - 1):
            bigram = word[i : i + 2]
            if bigram not in bigram_db:
                bigram_db[bigram] = set()
            bigram_db[bigram].add(word)

    def get_words_with_bigram(self, bigram):
        return self.bigram_db[bigram] if bigram in self.bigram_db else []

    def index_len(self):
        return len(json.dumps(self.index))

    @staticmethod
    def edit_distance(s1, s2):
        m = len(s1) + 1
        n = len(s2) + 1

        tbl = {}
        for i in range(m):
            tbl[i, 0] = i
        for j in range(n):
            tbl[0, j] = j
        for i in range(1, m):
            for j in range(1, n):
                cost = 0 if s1[i - 1] == s2[j - 1] else 1
                rmcost = 1
                tbl[i, j] = min(
                    tbl[i, j - 1] + rmcost,
                    tbl[i - 1, j] + rmcost,
                    tbl[i - 1, j - 1] + cost,
                )

        return tbl[i, j]

    @staticmethod
    def normalize(v):
        norm = np.linalg.norm(v)
        if norm == 0:
            return v
        return v / norm

    def df(self, term):
        if term not in self.index:
            return 0
        return len(self.index[term])

    def tfd(self, term, doc_id, part):
        if (
            term in self.index
            and doc_id in self.index[term]
            and part in self.index[term][doc_id]
        ):
            return 1 + log(len(self.index[term][doc_id][part]))
        return 0

    def tfq(self, term, query):
        return query.count(term)

    def search(self, query, window_size=None, weight=2, category=None):
        query = self.correct_query(query)
        query = self.prepare_text(query)

        valid_docs = set()
        invalid_docs = set()

        def is_valid(doc):
            if not any([window_size, category and self.supports_categorization()]):
                return True

            if doc in invalid_docs:
                return False

            if doc in valid_docs:
                return True

            is_valid_doc = True

            if (window_size and not self.are_in_a_window(query, window_size, doc)) or (
                category and self.predict_category(doc) != category
            ):
                is_valid_doc = False

            if is_valid_doc:
                valid_docs.add(doc)
                return True
            else:
                invalid_docs.add(doc)
                return False

        doc_title_scores = {}
        doc_text_scores = {}
        query_score = np.zeros(len(query))
        for term_id in range(len(query)):
            term = query[term_id]
            query_score[term_id] = 1 + log(self.tfq(term, query))
            if term in self.index:
                dft = log(len(self.corpus) / self.df(term))
                query_score[term_id] = query_score[term_id] * dft
                for doc in self.index[term]:
                    if not is_valid(doc):
                        continue

                    if doc not in doc_title_scores:
                        doc_title_scores[doc] = np.zeros(len(query))
                        doc_text_scores[doc] = np.zeros(len(query))
                    title_score = self.tfd(term, doc, "title")
                    text_score = self.tfd(term, doc, "text")
                    doc_text_scores[doc][term_id] = text_score
                    doc_title_scores[doc][term_id] = title_score
            else:
                query_score[term_id] = 0

        query_score = SearchEngine.normalize(query_score)
        logger.debug(f"query score: {query_score}")

        for doc in doc_title_scores:
            doc_title_scores[doc] = SearchEngine.normalize(doc_title_scores[doc])
            doc_text_scores[doc] = SearchEngine.normalize(doc_text_scores[doc])

        relevant_docs = []
        for doc in doc_title_scores:
            score = np.dot(doc_title_scores[doc], query_score) * weight + np.dot(
                doc_text_scores[doc], query_score
            )
            relevant_docs.append((score, doc))
        list.sort(relevant_docs, reverse=True)
        logger.debug(f"tf-idf results: {str(relevant_docs)[:100]}")
        return [x[1] for x in relevant_docs]

    def are_in_a_window(self, query: List[str], window_size: int, doc_id) -> bool:
        for section in ["title", "text"]:
            if SearchEngine.find_tuple_in_window(
                [self.index[word].get(doc_id, dict()).get(section) for word in query],
                window_size,
            ):
                return True

        return False

    @staticmethod
    def find_tuple_in_window(indices_lists: List[List[int]], w_size: int):
        if any(l is None for l in indices_lists) or any(
            len(l) == 0 for l in indices_lists
        ):
            return None

        iters = [iter(l) for l in indices_lists]
        current = [next(i) for i in iters]
        while True:
            if max(current) - min(current) <= w_size:
                return tuple(current)
            else:
                min_i = min(range(len(current)), key=lambda i: current[i])
                try:
                    current[min_i] = next(iters[min_i])
                except StopIteration:
                    return None
