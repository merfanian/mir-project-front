# -*- encoding: utf-8 -*-
"""
License: MIT
Copyright (c) 2019 - present AppSeed.us
"""

from django.urls import path, re_path
from app import views
from . import persian_search_engine, english_search_engine

urlpatterns = [
    # Matches any html file
    path("search", views.search, name="pages"),
    # The home page
    path("", views.index, name="home"),
]

searchengine_en = english_search_engine.get_search_engine()
searchengine_fa = persian_search_engine.get_search_engine()