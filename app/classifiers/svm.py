from .classifier import *
from sklearn import svm


class SVMClassifier(Classifier):
    def __init__(self) -> None:
        super().__init__()
        self.trained_model = None

    def predict(self, title: list, text: list, **kwargs):
        data = vectorize_counted(count_terms(title + text))
        return self.trained_model.predict(data.reshape((1, len(data))))[0]

    def fit(self, **kwargs):
        super().fit(**kwargs)
        c = svm.SVC(1)  # Best according to the experiments
        train_data = get_vectorized_train_set()
        c.fit(train_data[TEXT_COLUMN].tolist(), train_data[TARGET_COLUMN].tolist())
        self.trained_model = c
