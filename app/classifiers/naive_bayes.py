import numpy as np
import pandas as pd
from .classifier import *


class NaiveBayesClassifier(Classifier):

    P_COLUMN = "p"

    def __init__(self) -> None:
        super().__init__()
        self.__trained_model = None

    def predict(self, title: list, text: list, **kwargs):
        def calc_probability(target, doc_counts):
            def get_probability(term):
                try:
                    return self.__trained_model.loc[(target, term)][
                        NaiveBayesClassifier.P_COLUMN
                    ]
                except:
                    return 0

            return sum(
                get_probability(term) * count for term, count in doc_counts.items()
            )

        return max(
            [-1, 1],
            key=lambda target: calc_probability(target, count_terms(text)),
        )

    def fit(self, **kwargs):
        super().fit(csv_path=kwargs.get("csv_path"))

        def sum_counts(counted_docs):
            totals = {term: 0 for term, _ in get_corpus()}
            for doc_counts in counted_docs:
                for term, count in doc_counts.items():
                    totals[term] = totals.get(term, 0) + count
            return list(totals.items())

        # Finding total counts in each class
        count_sums = (
            get_counted_train_set()
            .groupby(TARGET_COLUMN)
            .agg({TEXT_COLUMN: sum_counts})
        )
        count_sums["total"] = count_sums[TEXT_COLUMN].map(
            lambda count_sum: sum(count[1] for count in count_sum)
        )
        # Expand dictionaries
        count_sums = count_sums.explode(TEXT_COLUMN)
        count_sums[["term", "count"]] = pd.DataFrame(
            count_sums[TEXT_COLUMN].tolist(), index=count_sums.index
        )
        count_sums = count_sums.drop([TEXT_COLUMN], axis=1).set_index(
            ["term"], append=True
        )
        count_sums[NaiveBayesClassifier.P_COLUMN] = np.log(
            (count_sums["count"] + 1) / (count_sums["total"] + len(get_corpus()))
        )
        self.__trained_model = count_sums.drop(["total", "count"], axis=1)
