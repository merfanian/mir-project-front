import numpy as np
from .classifier import *


class KnnClassifier(Classifier):
    def predict(self, title: list, text: list, **kwargs):
        data = vectorize_counted(count_terms(title + text))
        neighbors = [
            (KnnClassifier.distance(data, v["description"]), v["views"])
            for (_, v) in get_vectorized_train_set().iterrows()
        ]
        neighbors.sort()
        # Based on experiments
        return KnnClassifier.get_best_fit([label for (d, label) in neighbors[0:1]])

    def fit(self, **kwargs):
        super().fit(**kwargs)
        pass

    @staticmethod
    def distance(v1, v2):
        return np.linalg.norm(v1 - v2)

    @staticmethod
    def get_best_fit(neighbors):
        return np.sign(sum(neighbors))