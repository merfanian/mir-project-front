import numpy as np
import logging

from . import knn, svm, random_forest, naive_bayes
from .classifier import Classifier
from django.conf import settings

__all__ = ["knn", "naive_bayes", "random_forest", "svm"]

knn = knn.KnnClassifier()
naive_bayes = naive_bayes.NaiveBayesClassifier()
random_forest = random_forest.RandomForestClassifier()
svm = svm.SVMClassifier()
logger = logging.getLogger(__name__)


class WeightedClassifier:
    def __init__(self, csv_path):
        self.weights = settings.CLASSIFIERS_WEIGHT
        logger.info(f"classifiers weight initialized, weights are: {self.weights}")
        logger.info("fitting naive bayes")
        naive_bayes.fit(csv_path=csv_path)
        logger.info("fitting random_forest")
        random_forest.fit(csv_path=csv_path)
        logger.info("fitting svm")
        svm.fit(csv_path=csv_path)

    def predict(self, title: list, text: list, **kwargs):
        global naive_bayes, knn, random_forest, svm

        classifiers = {
            "naive_bayes": naive_bayes,
            "svm": svm,
            "random_forest": random_forest,
            "knn": knn,
        }

        res = np.sign(
            np.average(
                [
                    classifiers[key].predict(title, text, **kwargs) * weight
                    for key, weight in self.weights.items()
                    if weight != 0
                ]
            )
        )
        logger.info(f"document predicted, class is: {res}")
        return res