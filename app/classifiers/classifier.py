from abc import ABC, abstractmethod
from sys import path
import numpy as np
import pandas as pd

TEXT_COLUMN = "text"
TARGET_COLUMN = "views"

__raw_train_set = None
__normalized_train_set = None
__counted_train_set = None
__vectorized_train_set = None

__corpus = None

__idfs = None


def load_raw_data(path: str):
    global __raw_train_set

    if __raw_train_set is not None:
        return

    def remove_unwanted_columns(df) -> pd.DataFrame:
        return df[["title", "description", "views"]]

    __raw_train_set = remove_unwanted_columns(pd.read_csv(path))
    __raw_train_set[TEXT_COLUMN] = __raw_train_set["title"] + __raw_train_set["description"]


def make_normalized_data():
    global __normalized_train_set

    def normalize_text(raw_text: str) -> str:
        from nltk import word_tokenize, WordNetLemmatizer
        from nltk.corpus import stopwords

        tokens = word_tokenize(raw_text)

        badc = set(".=+-*:!،؛؟»])},«\[({|?;")
        tokens = ["".join([char for char in w if char not in badc]) for w in tokens]

        lemma = WordNetLemmatizer()
        tokens = [lemma.lemmatize(word, pos="v") for word in tokens]
        tokens = [lemma.lemmatize(word, pos="n") for word in tokens]

        stop_words = set(stopwords.words("english"))

        tokens = [
            word.lower()
            for word in tokens
            if len(word) > 1 and word.isalpha() and word not in stop_words
        ]

        return tokens

    def normalize_set(df: pd.DataFrame):
        df = df.copy()
        df[TEXT_COLUMN] = df[TEXT_COLUMN].map(normalize_text)
        return df

    __normalized_train_set = normalize_set(__raw_train_set)


def count_terms(terms_list: list):
    counts = dict()
    for term in terms_list:
        counts[term] = counts.get(term, 0) + 1
    return counts


def make_counted_data():
    global __counted_train_set

    def count_terms_of_set(df: pd.DataFrame):
        df = df.copy()
        df[TEXT_COLUMN] = df[TEXT_COLUMN].map(count_terms)
        return df

    __counted_train_set = count_terms_of_set(get_normalized_train_set())


def make_corpus_of_data():
    global __corpus

    counted_train_set = get_counted_train_set()
    corpus = dict()
    for terms_list in counted_train_set[TEXT_COLUMN]:
        for term, count in terms_list.items():
            df, total = corpus.get(term, (0, 0))
            corpus[term] = (df + 1, total + count)
    __corpus = sorted(corpus.items())


def calc_idfs():
    global __idfs

    corpus = get_corpus()
    n = len(__counted_train_set)
    __idfs = [(term, np.log(n / df)) for term, (df, _) in corpus]


def vectorize_counted(term_counts: dict) -> np.ndarray:
    idfs = get_idfs()

    return np.fromiter(
        (term_counts.get(term, 0) * idf for term, idf in idfs),
        float,
    )


def make_vectorized_data():
    global __vectorized_train_set

    def vectorize_set(df: pd.DataFrame):
        df = df.copy()
        df[TEXT_COLUMN] = df[TEXT_COLUMN].map(vectorize_counted)
        return df

    __vectorized_train_set = vectorize_set(get_counted_train_set())


def get_normalized_train_set():
    if __normalized_train_set is None:
        make_normalized_data()
    return __normalized_train_set


def get_counted_train_set():
    if __counted_train_set is None:
        make_counted_data()
    return __counted_train_set


def get_vectorized_train_set():
    if __vectorized_train_set is None:
        make_vectorized_data()
    return __vectorized_train_set


def get_corpus():
    if __corpus is None:
        make_corpus_of_data()
    return __corpus


def get_idfs():
    if __idfs is None:
        calc_idfs()
    return __idfs


class Classifier(ABC):
    def __init__(self) -> None:
        super().__init__()

    @abstractmethod
    def predict(self, title: list, text: list, **kwargs):
        pass

    def fit(self, **kwargs):
        load_raw_data(kwargs.get("csv_path"))
