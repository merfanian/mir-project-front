# -*- encoding: utf-8 -*-
"""
License: MIT
Copyright (c) 2019 - present AppSeed.us
"""

from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404, redirect
from django.template import loader
from django.http import HttpResponse
from django.views.decorators.cache import cache_page
from django import template
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django.conf import settings

from . import persian_search_engine, english_search_engine

from nltk.corpus import words

import logging

CACHE_TTL = getattr(settings, "CACHE_TTL", DEFAULT_TIMEOUT)


logger = logging.getLogger(__name__)


en_se = english_search_engine.get_search_engine()
fa_se = persian_search_engine.get_search_engine()


def index(request):
    return render(request, "index.html")


@cache_page(CACHE_TTL)
def search(request):
    args = {}
    query = str(request.GET.get("query")).replace("_", " ")
    window_size = request.GET.get("window_size", None)
    if not window_size:
        window_size = "None"
    ws = int(window_size) if window_size != "None" else None
    show_most_viewed = int(request.GET.get("class", None))
    show_most_viewed = show_most_viewed if show_most_viewed != 0 else None
    logger.info(
        f"New search request, query is: {query}, window_size is: {ws}, category is: {show_most_viewed}"
    )

    if is_english(query.rsplit(" ")[0]):
        se = en_se
    else:
        se = fa_se

    result = se.search(query, window_size=ws, category=show_most_viewed)

    title_snippets = [get_snippet(se.get_page_by_id(res)[0]) for res in result]
    text_snippets = [get_snippet(se.get_page_by_id(res)[1]) for res in result]

    args["titles"], args["texts"] = title_snippets[:10], text_snippets[:10]
    args["results"] = [
        (get_snippet(page[0]), get_snippet(page[1]))
        for page in [se.get_page_by_id(res) for res in result]
    ]
    args["range"] = range(10)
    args["query"] = str(query).replace(" ", "_")
    args["window_size"] = ws

    return render(request, "search.html", args)


def get_snippet(text):
    snippet = text.replace("\n", " ")
    return snippet[:200] + "..."


def is_english(s):
    try:
        s.encode(encoding="utf-8").decode("ascii")
    except UnicodeDecodeError:
        return False
    else:
        return True