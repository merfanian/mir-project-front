#!/usr/bin/env python
# coding: utf-8

from __future__ import unicode_literals
from app.encoding import Encoder, GammaEncoder
import xml.etree.ElementTree as ET
import logging
import string
from hazm import *
from .searchengine import SearchEngine
from django.conf import settings

logger = logging.getLogger(__name__)

search_engine = None

fast_import = settings.FAST_IMPORT


def get_search_engine():
    global search_engine
    if search_engine == None:
        search_engine = PersianSearchEngine(
            "core/static/data/Persian.xml",
            "core/static/export/Persian.json",
            "core/static/stopwords/persian_stopwords.txt",
            GammaEncoder(),
        )
    return search_engine


class PersianSearchEngine(SearchEngine):
    def __init__(
        self, docs_path: str, export_path: str, stopwords_path: str, encoder: Encoder
    ):
        super().__init__(docs_path, export_path, stopwords_path, encoder)

    @staticmethod
    def is_farsi(word):
        farsi_chars = [chr(c) for c in range(ord("\u0600"), ord("\u06FF"))]
        for c in word:
            if c in farsi_chars:
                return True
        return False

    def prepare_text(self, raw_text):
        prepared_text = raw_text
        prepared_text = prepared_text.replace("\u200c", "")  # semi-space removal

        normalizer = Normalizer()
        prepared_text = normalizer.normalize(prepared_text)
        l = word_tokenize(prepared_text)

        for word in l:
            self.add_word_to_bigram(word)

        stemmer = Stemmer()
        l = [stemmer.stem(x) for x in l]

        l = [word for word in l if not word in self.stopwords]

        badc = ".=+-*:!،؛؟»])},«\[({|#_"
        bads = [c for c in badc]

        for char in string.punctuation:
            bads.append(char)

        for char in string.ascii_letters:
            bads.append(char)

        l = ["".join([char for char in w if char not in bads]) for w in l]
        l = [w for w in l if len(w) > 0 and PersianSearchEngine.is_farsi(w)]

        logger.debug(f"persian | prepared text is:{str(l)[:30]}")
        return l

    def get_corpus(self, docs_path):
        tree = ET.parse(docs_path)
        self.root = tree.getroot()
        self.corpus = {}
        self.bigram_db.clear()
        cnt = 0
        for movie in self.root.iter("page"):
            title = ""
            text = ""
            id = 0
            for t in movie.iter("title"):
                title = t.text
            for t in movie.iter("text"):
                text = t.text
            for t in movie.iter("id"):
                id = int(t.text)
                break
            title = self.prepare_text(title)
            text = self.prepare_text(text)
            if id in self.corpus:
                logger.warn(f"Id {id} already exists")
            self.corpus[id] = (title, text)
            cnt += 1
            if fast_import and cnt >= 100:
                break

        return self.corpus

    def correct_query(self, query):
        suggestions = []

        normalizer = Normalizer()
        prepared_text = normalizer.normalize(query)
        qlist = word_tokenize(prepared_text)

        for word in qlist:
            if word not in self.words_db and len(word) > 1:
                candidates = {}
                for i in range(len(word) - 1):
                    b = word[i : i + 2]
                    wl = self.get_words_with_bigram(b)
                    for w in wl:
                        if w not in candidates:
                            candidates[w] = 0
                        candidates[w] = candidates[w] + 1

                score = {}
                mx = 0
                mxlist = []
                for w in candidates:
                    inter = candidates[w]
                    union = len(w) + len(word) - 2 - inter
                    score = inter / union
                    if score > mx:
                        mx = score
                        mxlist = []
                    if score == mx:
                        mxlist.append(w)

                mxx = 1000000
                correct = mxlist[0]
                for w in mxlist:
                    if SearchEngine.edit_distance(w, word) < mxx:
                        mxx = SearchEngine.edit_distance(w, word)
                        correct = w
                suggestions.append((word, correct))

        correct_query = query

        for (bad, good) in suggestions:
            correct_query = correct_query.replace(bad, good)

        logger.debug(f"query was: {query} and corrected to: {correct_query}")
        return correct_query

    def get_page_by_id(self, id):
        for movie in self.root.iter("page"):
            title = ""
            text = ""
            pid = 0
            for t in movie.iter("title"):
                title = t.text
            for t in movie.iter("text"):
                text = t.text
            for t in movie.iter("id"):
                pid = int(t.text)
                break
            if id == pid:
                return (str(id) + ": " + title, text)

    def supports_categorization(self) -> bool:
        return False

    def predict_category(self, doc) -> int:
        return 0
