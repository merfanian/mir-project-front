from abc import ABC, abstractmethod
from typing import Iterable


class Encoder(ABC):
    def __init__(self, has_leading_zeros) -> None:
        self.has_leading_zeros = has_leading_zeros

    @abstractmethod
    def encode(self, num: int) -> str:
        pass

    @abstractmethod
    def decode(self, encoded: bytes) -> Iterable[int]:
        pass


class GammaEncoder(Encoder):
    def __init__(self) -> None:
        super().__init__(True)

    def encode(self, num: int) -> str:
        bits = bin(num)[2 + 1 :]
        return "1" * len(bits) + "0" + bits

    def decode(self, encoded: bytes) -> Iterable[int]:
        one_count = 0
        i = 0
        g_str = bin(int.from_bytes(encoded, byteorder="big"))[2:]
        if self.has_leading_zeros:
            g_str = g_str[1:]
        while i < len(g_str):
            if g_str[i] == "1":
                one_count += 1
            else:
                yield int("1" + g_str[i + 1 : i + one_count + 1], 2)
                i += one_count
                one_count = 0
            i += 1


class VariableLengthEncoder(Encoder):
    def __init__(self) -> None:
        super().__init__(False)

    def encode(self, num: int) -> str:
        bits = bin(num)[2:]
        chunks = [bits[max(0, i - 7) : i] for i in range(len(bits), -1, -7)]
        chunks.reverse()
        chunks[0] = chunks[0].zfill(7)
        for i in range(len(chunks) - 1):
            chunks[i] = "0" + chunks[i]
        chunks[-1] = "1" + chunks[-1]
        return "".join(chunks)

    def decode(self, encoded: bytes) -> Iterable[int]:
        current_num = 0
        for byte in encoded:
            current_num += byte & 0x7F
            if byte < 128:
                current_num <<= 7
            else:
                yield current_num
                current_num = 0


def convert_bin_str_to_bytes(bin_str):
    result = int(bin_str, 2)
    return result.to_bytes((result.bit_length() + 7) // 8, byteorder="big")


x = {"100": {"title": [1, 2, 3, 4], "text": [0]}, "200": {"text": [0, 1000]}}
