#!/usr/bin/env python
# coding: utf-8

from __future__ import unicode_literals
from app.classifiers.classifier import Classifier
from app.classifiers import WeightedClassifier
from app.encoding import Encoder, GammaEncoder
from nltk import word_tokenize
from nltk.corpus import stopwords
from nltk import WordNetLemmatizer
from django.conf import settings
import csv

import numpy as np
import logging

logger = logging.getLogger(__name__)

fast_import = settings.FAST_IMPORT

# import ntlk
from .searchengine import SearchEngine

search_engine = None


def get_search_engine():
    global search_engine
    if search_engine == None:
        search_engine = EnglishSearchEngine(
            "core/static/data/ted_talks.csv",
            "core/static/export/English.json",
            "core/static/stopwords/english_stopwords.txt",
            GammaEncoder(),
            WeightedClassifier("core/static/classifiers/train.csv"),
        )
    return search_engine


class EnglishSearchEngine(SearchEngine):
    def __init__(
        self,
        docs_path: str,
        export_path: str,
        stopwords_path: str,
        encoder: Encoder,
        classifier: Classifier,
    ):
        super().__init__(docs_path, export_path, stopwords_path, encoder)
        self.classifier = classifier

    @staticmethod
    def is_english(s):
        try:
            s.encode(encoding="utf-8").decode("ascii")
        except UnicodeDecodeError:
            return False
        else:
            return True

    def prepare_text(self, raw_text):
        tokens = word_tokenize(raw_text)

        tokens = [word for word in tokens if word.isalpha()]

        tokens = [word.lower() for word in tokens]

        for w in tokens:
            self.add_word_to_bigram(w)

        tokens = [word for word in tokens if not word in self.stopwords]

        lemma = WordNetLemmatizer()
        tokens = [lemma.lemmatize(word, pos="v") for word in tokens]
        tokens = [lemma.lemmatize(word, pos="n") for word in tokens]

        badc = ".=+-*:!،؛؟»])},«\[({|"

        bads = [c for c in badc]

        tokens = ["".join([char for char in w if char not in badc]) for w in tokens]
        tokens = [w for w in tokens if len(w) > 0 and EnglishSearchEngine.is_english(w)]

        logger.debug(
            f"raw text is: {raw_text[:30]}| prepared text is:{str(tokens)[:30]}"
        )
        return tokens

    def get_corpus(self, docs_path):
        title_index = 14
        desc_index = 1
        self.corpus = {}
        self.bigram_db.clear()
        self.titles, self.descs = [], []
        with open(docs_path, "r") as file:
            read_csv = csv.reader(file)
            i = 0
            for line in read_csv:
                title = self.prepare_text(line[title_index])
                desc = self.prepare_text(line[desc_index])
                self.titles.append(line[title_index])
                self.descs.append(line[desc_index])

                if i in self.corpus:
                    logger.warn(f"Id {i} already exists")

                self.corpus[i] = (title, desc)
                i += 1

                if fast_import and i >= 100:
                    break

        return self.corpus

    def correct_query(self, query):

        suggestions = []

        tokens = word_tokenize(query)

        tokens = [word for word in tokens if word.isalpha()]

        tokens = [word.lower() for word in tokens]

        qlist = tokens

        for word in qlist:
            if word not in self.words_db and len(word) > 1:
                candidates = {}
                for i in range(len(word) - 1):
                    b = word[i : i + 2]
                    wl = self.get_words_with_bigram(b)
                    for w in wl:
                        if w not in candidates:
                            candidates[w] = 0
                        candidates[w] = candidates[w] + 1

                score = {}
                mx = 0
                mxlist = []
                for w in candidates:
                    inter = candidates[w]
                    union = len(w) + len(word) - 2 - inter
                    score = inter / union
                    if score > mx:
                        mx = score
                        mxlist = []
                    if score == mx:
                        mxlist.append(w)

                mxx = 1000000
                correct = mxlist[0]
                for w in mxlist:
                    if SearchEngine.edit_distance(w, word) < mxx:
                        mxx = SearchEngine.edit_distance(w, word)
                        correct = w
                suggestions.append((word, correct))

        correct_query = query

        for (bad, good) in suggestions:
            correct_query = correct_query.replace(bad, good)

        logger.debug(f"query was: {query} and corrected to: {correct_query}")
        return correct_query

    def get_page_by_id(self, id):
        return (str(id) + ": " + self.titles[id], self.descs[id])

    def supports_categorization(self) -> bool:
        return True

    def predict_category(self, doc) -> int:
        doc = self.corpus[doc]
        return self.classifier.predict(title=doc[0], text=doc[1])
